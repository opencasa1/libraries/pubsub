const pubsub = require("../index.js");

describe("> pubsub", () => {
  let events;

  beforeEach(() => {
    events = pubsub();
  });

  test("subscribing/unsubscribing from a channel must not leave the listener put", () => {
    const listener = jest.fn();
    const dispose = events.subscribe("fooChannel", listener);

    expect(events.getChannels().includes("fooChannel")).toBe(true);

    dispose();
    events.publish("fooChannel", Math.random());

    expect(listener).not.toBeCalled();
  });

  test("reseting all channels will no trigger any observer", () => {
    const listener1 = jest.fn();
    const listener2 = jest.fn();
    const listener3 = jest.fn();

    events.subscribe("event1", listener1);
    events.subscribe("event2", listener2);
    events.subscribe("event3", listener3);

    events.resetChannels();

    events.publish("event1", Math.random());
    events.publish("event2", Math.random());
    events.publish("event3", Math.random());

    expect(listener1).not.toBeCalled();
    expect(listener2).not.toBeCalled();
    expect(listener3).not.toBeCalled();
  });

  test("publishing in a channel will trigger all subscribed listeners", () => {
    const listener1 = jest.fn();
    const listener2 = jest.fn();

    events.subscribe("event1", listener1);
    events.subscribe("event1", listener2);
    events.publish("event1", "hello world");

    expect(listener1).toBeCalledWith("hello world");
    expect(listener2).toBeCalledWith("hello world");
  });

  test("publishing to a channel without listeners will do nothing", () => {
    const listener1 = jest.fn();
    events.subscribe("event1", listener1);

    events.publish("unexistingEvent", "hello world");
    expect(listener1).not.toBeCalled();
  });

  test("reseting a specific channel will only dispose its event listeners", () => {
    const listener1 = jest.fn();
    const listener2 = jest.fn();

    events.subscribe("event1", listener1);
    events.subscribe("event2", listener2);
    events.resetChannels("event1");
    events.publish("event2", "hello world");

    expect(listener1).not.toBeCalled();
    expect(listener2).toBeCalledWith("hello world");
  });

  test("subscribing to new channel using on method", () => {
    const listener1 = jest.fn();
    events.on("foobar", listener1);
    events.publish("foobar", "priscilla");
    expect(listener1).toBeCalledWith("priscilla");
  });

  test("simulating browser environment", () => {
    global.window = {
      location: {
        hash: "debug",
      },
    };
    const ps = pubsub();
    const listener = jest.fn();
    ps.on("dunno", listener);
    ps.publish("dunno", "hello", "priscilla");
    expect(listener).toBeCalledWith("hello", "priscilla");
  });
});
