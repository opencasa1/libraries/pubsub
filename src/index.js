function pubsub(debug) {
  const channels = {};
  const logOperation = debug ? console.debug : () => null;

  return {
    publish(channel, ...args) {
      if (!channels[channel]) {
        return;
      }

      logOperation(`publish event ${channel} with args`, args);

      for (const listener of channels[channel]) {
        listener(...args);
      }
    },
    subscribe(channel, listener) {
      if (!channels[channel]) {
        channels[channel] = [];
      }

      logOperation(`subscribing to event ${channel}`);
      channels[channel].push(listener);

      return () => {
        channels[channel] = channels[channel].filter(
          (observer) => observer !== listener,
        );
      };
    },
    on(channel, listener) {
      return this.subscribe(channel, listener);
    },
    resetChannels(...args) {
      args = args.length ? args : this.channels;

      for (const channelName of args) {
        channels[channelName].length = 0;
      }
    },
    getChannels() {
      return this.channels;
    },
    get channels() {
      return Object.keys(channels);
    },
  };
}

module.exports = function wrapper() {
  let debug;

  if (typeof window !== "undefined") {
    debug = window.location.hash.includes("debug");
  } else {
    debug = process.env.PUBSUB_DEBUG === "true";
  }

  return pubsub(debug);
};
