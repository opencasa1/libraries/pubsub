## 📝 Descripción.

> Descripción de los elementos cambiados y explicación de estos.
> Tambien considerar puntos criticos a revisar.

## ✅ Validacion del cambio.

> Agregar imagenes en caso de aplicar donde se muestre un antes y despues (UI) en
> el caso de ser aun cambio en respuesta o uso de una API deben estar los curls
> para validar dichos cambios.

|antes|despues|
|-|-|
| | |

## ☑ Check List

- [ ] Se agregaron o ajustaron pruebas unitarias.
- [ ] Se agrego documentación relativa al cambio.
- [ ] Se valido que el cambio no genere incosistencias.
- [ ] Es seguro mezclar si generar errores en otros proyectos.
