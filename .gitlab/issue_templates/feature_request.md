## 📝 Descripción.

> Agregar toda informacion relevante para la replica del error
> observaciones en caso de ser necesarias.

## 🎇 Evidencia / Guia.

> Agregar imagenes o gif que ilustren el problema o cualquier otro medio por el 
> que se pueda dar entendimiento del caso.

## 🚧 Datos relevantes.

- Version APP : <>
- Ambiente : <>
- Version DB : <>

> algun otro dato relevante que se pueda añadir aparte de la version de la app o 
> el ambiente.